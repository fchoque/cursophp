<?php 
    include('../../rutas/route.php');

    $obj = new TramiteController();
    $tramites = $obj->listar();
?>
<?php 
    include('../templates/app.php');  
?>


<div class="container">
    <h1>Lista de tramites </h1>
    <hr>
    <button class="btn btn-default" >Buscar</button>
    <button class="btn btn-primary" >Buscar</button>
    <button class="btn btn-success" >Buscar</button>
    <button class="btn btn-danger" >Buscar</button>
    <button class="btn btn-warning" >Buscar</button>
    <button class="btn btn-info" >Buscar</button>

<hr>

<p class="text-right">
    <button class="btn btn-lg btn-primary">
        <span class="glyphicon glyphicon-plus"></span>
        Nuevo Tramite
    </button>
</p>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Lista</div>


  <!-- Table -->
  <table class="table">
    <tr>
        <th>Id</th>
        <th>Nro</th>
        <th>Nombre</th>
        <th>Opciones</th>
    </tr>

    <?php while($row = $tramites->fetch_object()): ?>
    <tr>
        <td> <?php echo $row->id ?> </td>
        <td> <?php echo $row->numero ?> </td>
        <td> <?php echo $row->nombre ?> </td>

        <td>
            <a href="show.php?idp=<?php echo $row->id ?>" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-eye-open"></span> Ver </a>
            <a href="" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-edit"></span> Modificar</a>
            <a href="eliminar.php?idp=<?php echo $row->id ?>"class="btn btn-sm btn-danger" onclick="return confirm('estas seguro?')"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
        </td>
    </tr>
    <?php endwhile; ?>

  </table>
</div>

</div>

<?php 
    include('../templates/footer.php');  
?>


