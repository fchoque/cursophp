<?php 
    include('../../rutas/route.php');
    //Recogiendo variables desde la url GET
    $id = $_GET['idp'];
    $obj = new TramiteController();
    $resultado = $obj->mostrar($id);

    $tramite= $resultado->fetch_object();
    


    
?>
<?php 
    include('../templates/app.php');  
?>
    <div class="container">
        <hr>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <h1>Detalle</h1>
                <hr>
                <ul class="list-group">
                    <li class="list-group-item"> <strong>Id: </strong><?php echo $tramite->id ?></li>
                    <li class="list-group-item"> <strong>Nro: </strong><?php echo $tramite->numero ?></li>
                    <li class="list-group-item"> <strong>Nombre: </strong><?php echo $tramite->nombre ?></li>
                </ul>    
                <hr>
                <a href="./" class="btn btn-md btn-default">Volver</a>                
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
<?php 
    include('../templates/footer.php');  
?>