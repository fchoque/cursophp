<?php 

class TramiteController
{
    private function ejecutar_consulta($query)
    {
        $cadcon = Conexion::conectar();
        $res = $cadcon->query($query);
        Conexion::desconectar($cadcon);

        return $res;
    }

    public function listar()
    {
        $query = "SELECT * FROM tramite";
        return $this->ejecutar_consulta($query);
    }

    public function mostrar($id)
    {
        $query = "SELECT * FROM tramite WHERE id = '$id'";
        return $this->ejecutar_consulta($query);
    }

    public function eliminar($id)
    {
        $query = "DELETE FROM tramite WHERE id = '$id'";
        return $this->ejecutar_consulta($query);
    }
}


/*$obj = new TramiteController();
$tramites = $obj->listar();
print_r($tramites);
*/